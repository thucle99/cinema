import {
  Controller,
  Get,
  Post,
  Render,
  Res,
  Req,
  Request,
  Query,
} from '@nestjs/common';
import { Response } from 'express';
import { Nhanvien } from './model/Connguoi/Nhanvien';
import { Ve } from './model/RapPhim/Ve';
import { GheService } from './service/Ghe.service';
import { HoadonphatService } from './service/Hoadonphat.service';
import { LichchieuService } from './service/Lichchieu.service';
import { NhanvienService } from './service/Nhanvien.service';
import { PhimService } from './service/Phim.service';
import { PhongchieuService } from './service/Phongchieu.service';
import { VeService } from './service/Ve.service';

@Controller()
export class AppController {
  constructor(
    private readonly nhanvienService: NhanvienService,
    private readonly lichchieuService: LichchieuService,
    private readonly phimService: PhimService,
    private readonly phongchieuService: PhongchieuService,
    private readonly gheService: GheService,
    private readonly veService: VeService,
    private readonly hoadonphatService: HoadonphatService,
  ) { }
  public messageErr: string = "";
  public ghedatId: number;
  public ve: Ve;
  public nhanvien: Nhanvien;
  public taikhoan: string;
  public tienphat: number = 0;

  @Get()
  @Render('GDTrangchu')
  async trangchu() {
    return {
      taikhoan: this.nhanvien?.taikhoan
    }
  }

  @Get('dang-nhap')  // phuong thuc
  @Render('GDDangnhap')  // chon file html
  async dangnhap() {
    return { messageErr: this.messageErr }
  }

  @Post('/dang-nhap')
  async kiemtraDangnhap(@Res() res: Response, @Req() req: Request) {
    return await this.nhanvienService.kiemtraDangnhap(req.body)
      .then(result => {
        if (result) {
          this.nhanvien = result;
          res.redirect('/');
        }
        else {
          this.messageErr = "Tài khoản hoặc mật khẩu không chính xác"
          res.redirect('/dang-nhap');
        }
      })
  }

  @Get('tra-ve-xem-phim')
  @Render('GDTravexemphim')
  async travexemphim(@Query() query) {
    let listLichchieu = query.phimId ?
      await this.lichchieuService.getLichchieuByPhimId(query.phimId) :
      await this.lichchieuService.getLichchieuByPhongId(query.phongchieuId)

    return {
      listPhim: await this.phimService.getListPhim(),
      listPhongchieu: await this.phongchieuService.getListPhongchieu(),
      listLichchieu: listLichchieu,
      phongchieuId: query.phongchieuId,
      phimId: query.phimId
    }
  }


  @Get('tra-ve-xem-phim/chon-so-ghe')
  @Render('GDChonsoghe')
  async chonsoghe(@Query() query) {
    return {
      listGhe: await this.gheService.
      getGheByPhongchieuId(query.phongchieuId),
      lichchieuId: query.lichchieuId,
      hang: query.hang,
      cot: query.cot
    }
  }

  @Get('tra-ve-xem-phim/hoa-don-phat')
  @Render('GDHoadonphat')
  async hoadonphat(@Query() query) {
    this.ve = await this.veService.getVeByGhechonId(query.ghedatId);
    this.taikhoan = query.taikhoan
    this.ghedatId=query.ghedatId
    let tongthoigian: string = this.handleTime(this.ve.ghedat.lichchieu.ngaygiochieu, this.ve.giave)
    
    return {
      ve: this.ve,
      tongthoigian: tongthoigian,
      tienphat: this.tienphat
    }
  }

  @Post('/tra-ve-xem-phim/hoa-don-phat')
  async luuHoadonphat(@Res() res: Response, @Req() req: Request) {
    this.hoadonphatService.luuHoadonphat(this.ghedatId, this.ve, this.taikhoan, this.tienphat)
    setTimeout(() => {
      res.redirect('/');
    }, 5000)
  }

  @Get('dang-ky')
  @Render('GDDangky')
  async dangky() { }

  @Post('/dang-ky')
  async dangKy(@Res() res: Response, @Req() req: Request) {
    await this.nhanvienService.dangky(req.body);
    res.redirect('/');
  }


  handleTime(created_at, giave) {
    let dateNow: any = new Date();
    let dateShow: any = new Date(created_at);
    let diffMinute = Math.floor((dateShow - dateNow) / 60000);
    let diffHour = Math.floor(diffMinute / 60);
    let diffDay = Math.floor(diffMinute / 1440);

    if (diffHour <= 6) {
      this.tienphat = giave;
      return diffHour + ' giờ';
    }
    if (diffHour > 6 && diffHour <= 12) {
      this.tienphat = giave * 0.6;
      return diffHour + ' giờ';
    }
    if (diffHour > 12 && diffHour <= 24) {
      this.tienphat = giave * 0.4;
      return diffHour + ' giờ';
    }
    else if (diffDay >= 1 && diffDay <= 2) {
      this.tienphat = giave * 0.2
      return diffDay + ' ngày ' + (diffHour - diffDay * 24) + ' giờ';
    }
    else if (diffDay > 2) {
      this.tienphat = 0;
      return diffDay + ' ngày ' + (diffHour - diffDay * 24) + ' giờ';
    }
  }

}
