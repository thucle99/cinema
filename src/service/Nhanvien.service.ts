import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Nhanvien } from '../model/Connguoi/Nhanvien';
import { Repository } from 'typeorm';

@Injectable()
export class NhanvienService {
  constructor(
    @InjectRepository(Nhanvien)
    private readonly nhanvienRepository: Repository<Nhanvien>,
  ) { }

  kiemtraDangnhap(data): Promise<Nhanvien> {
    return this.nhanvienRepository.findOne(data);
  }

  dangky(data: any): Promise<Nhanvien> {
    return this.nhanvienRepository.save(data);
  }

  getNhanvienByTaikhoan(taikhoan: string): Promise<Nhanvien> {
    return this.nhanvienRepository.findOne({
      where: { taikhoan: taikhoan },
    })
  }

}
