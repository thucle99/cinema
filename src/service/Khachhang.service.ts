import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Khachhang } from '../model/Connguoi/Khachhang';
import { Repository } from 'typeorm';

@Injectable()
export class KhachhangService {
  constructor(
    @InjectRepository(Khachhang)
    private readonly khachhangRepository: Repository<Khachhang>,
  ) { }

  async updateKhachhangById(khachhangId: number, diemtru: number): Promise<void> {
    let khachhangUpdate = await this.khachhangRepository.findOne(khachhangId);
    khachhangUpdate.diemtichluy = khachhangUpdate.diemtichluy - diemtru;
    this.khachhangRepository.save(khachhangUpdate)
  }

}
