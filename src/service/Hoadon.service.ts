import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Ghedat } from '../model/RapPhim/Ghedat';
import { Hoadon } from '../model/RapPhim/Hoadon';
import { Repository } from 'typeorm';

@Injectable()
export class HoadonService {
  constructor(
    @InjectRepository(Hoadon)
    private readonly hoadonService: Repository<Hoadon>,
  ) { }

  // getHoadonById(hoadonId: number): Promise<Hoadon> {
  //   return this.hoadonService.findOne({
  //     relations: ["nhanvien", "khachhang", "listVe"],
  //     where: { id: hoadonId },
  //   })
  // }

}
