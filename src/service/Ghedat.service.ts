import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Ghedat } from '../model/RapPhim/Ghedat';
import { Repository } from 'typeorm';

@Injectable()
export class GhedatService {
  constructor(
    @InjectRepository(Ghedat)
    private readonly ghedatRepository: Repository<Ghedat>,
  ) { }

  async updateGhedatById(ghedatId: number): Promise<void> {
    let ghedatUpdate = await this.ghedatRepository.findOne(ghedatId);
    ghedatUpdate.trangthai = "Trống";
    this.ghedatRepository.save(ghedatUpdate)
  }
}
