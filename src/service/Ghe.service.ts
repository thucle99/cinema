import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Ghe } from '../model/RapPhim/Ghe';
import { Repository } from 'typeorm';

@Injectable()
export class GheService {
  constructor(
    @InjectRepository(Ghe)
    private readonly gheRepository: Repository<Ghe>,
  ) { }

  getGheByPhongchieuId(phongchieuId: number): Promise<Ghe[]> {
    return this.gheRepository.find({
      relations: ["listGhedat", "listGhedat.lichchieu"],
      where: { phongchieu: { id: phongchieuId } },
      order: {
        id: "ASC"
      }
    })
  }

}
