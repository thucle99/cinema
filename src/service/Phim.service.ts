import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Phim } from '../model/RapPhim/Phim';
import { Repository } from 'typeorm';

@Injectable()
export class PhimService {
  constructor(
    @InjectRepository(Phim)
    private readonly phimRepository: Repository<Phim>,
  ) {}
  
  getListPhim():Promise<Phim[]> {
    return this.phimRepository.find();
  }

}
