import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Hoadonphat } from '../model/RapPhim/Hoadonphat';
import { Ve } from '../model/RapPhim/Ve';
import { Repository } from 'typeorm';
import { GhedatService } from './Ghedat.service';
import { KhachhangService } from './Khachhang.service';
import { NhanvienService } from './Nhanvien.service';
import { VeService } from './Ve.service';

@Injectable()
export class HoadonphatService {
  constructor(
    @InjectRepository(Hoadonphat)
    private readonly hoadonphatService: Repository<Hoadonphat>,
    private readonly ghedatService: GhedatService,
    private readonly veService: VeService,
    private readonly khachhangService: KhachhangService,
    private readonly nhanvienService: NhanvienService,
  ) { }

  async luuHoadonphat(ghedatId: number, ve: Ve, taikhoan: string, tienphat: number): Promise<void> {
    let nhanvien = await this.nhanvienService.getNhanvienByTaikhoan(taikhoan);
    let hoadonphat = new Hoadonphat();
    hoadonphat.soluongvetra = 1;
    hoadonphat.tienphat = tienphat;
    hoadonphat.tienhoanlai = ve.hoadon.tongtien - tienphat;
    hoadonphat.thoigianhoanve = new Date;
    hoadonphat.nhanvien = nhanvien;
    hoadonphat.hoadon = ve.hoadon;

    this.ghedatService.updateGhedatById(ghedatId);
    this.veService.deleteVeById(ve.id);
    this.khachhangService.updateKhachhangById(ve.hoadon.khachhang.id, ve.giave / 10000);
    this.hoadonphatService.save(hoadonphat);
  }

}
