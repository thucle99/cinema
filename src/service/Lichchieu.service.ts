import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Lichchieu } from '../model/RapPhim/Lichchieu';
import { Repository } from 'typeorm';

@Injectable()
export class LichchieuService {
  constructor(
    @InjectRepository(Lichchieu)
    private readonly lichchieuRepository: Repository<Lichchieu>,
  ) { }

  getLichchieuByPhimId(phimId: number):Promise<Lichchieu[]> {
    return this.lichchieuRepository.find({
      relations: ["phim", "phongchieu"],
      where: { phim: { id: phimId } },
      order: {
        ngaygiochieu:"ASC"
      }
    })
  }

  async getLichchieuByPhongId(phongId: number):Promise<Lichchieu[]>  {
    return this.lichchieuRepository.find({
      relations: ["phim", "phongchieu"],
      where: { phongchieu: { id: phongId } },
      order: {
        ngaygiochieu:"ASC"
      }
    })
  }

}
