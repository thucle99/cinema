import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Phongchieu } from '../model/RapPhim/Phongchieu';
import { Repository } from 'typeorm';

@Injectable()
export class PhongchieuService {
  constructor(
    @InjectRepository(Phongchieu)
    private readonly phongchieuRepository: Repository<Phongchieu>,
  ) { }

  getListPhongchieu():Promise<Phongchieu[]>  {
    return this.phongchieuRepository.find({
      order: {
        id:"ASC"
      }
    });
  }

}
