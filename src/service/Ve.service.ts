import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Ve } from '../model/RapPhim/Ve';
import { Repository } from 'typeorm';

@Injectable()
export class VeService {
  constructor(
    @InjectRepository(Ve)
    private readonly veRepository: Repository<Ve>,
  ) { }

  getVeByGhechonId(ghedatId: number): Promise<Ve> {
    return this.veRepository.findOne({
      relations: ["hoadon","hoadon.khachhang", "ghedat", "ghedat.ghe", "ghedat.lichchieu",
        "ghedat.lichchieu.phim", "ghedat.lichchieu.phongchieu"],
      where: { ghedat: { id: ghedatId } },
    })
  }

  deleteVeById(veId: number): void {
    this.veRepository.delete(veId);
  }
}
