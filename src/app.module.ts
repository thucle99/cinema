import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { Khachhang } from './model/Connguoi/Khachhang';
import { Nguoi } from './model/Connguoi/Nguoi';
import { Nguoidung } from './model/Connguoi/Nguoidung';
import { Nhanvien } from './model/Connguoi/Nhanvien';
import { Quanly } from './model/Connguoi/Quanly';
import { Dichvusudung } from './model/RapPhim/Dichvusudung';
import { Ghe } from './model/RapPhim/Ghe';
import { Ghedat } from './model/RapPhim/Ghedat';
import { Hoadon } from './model/RapPhim/Hoadon';
import { Hoadonphat } from './model/RapPhim/Hoadonphat';
import { Lichchieu } from './model/RapPhim/Lichchieu';
import { Mathang } from './model/RapPhim/Mathang';
import { Phim } from './model/RapPhim/Phim';
import { Phongchieu } from './model/RapPhim/Phongchieu';
import { Rapchieu } from './model/RapPhim/Rapchieu';
import { Ve } from './model/RapPhim/Ve';
import { GheService } from './service/Ghe.service';
import { GhedatService } from './service/Ghedat.service';
import { HoadonService } from './service/Hoadon.service';
import { HoadonphatService } from './service/Hoadonphat.service';
import { KhachhangService } from './service/Khachhang.service';
import { LichchieuService } from './service/Lichchieu.service';
import { NhanvienService } from './service/Nhanvien.service';
import { PhimService } from './service/Phim.service';
import { PhongchieuService } from './service/Phongchieu.service';
import { VeService } from './service/Ve.service';

@Module({
  imports: [
    ConfigModule.forRoot(),
    TypeOrmModule.forRoot({
      type: 'postgres',
      entities: [
        Quanly,
        Dichvusudung,
        Ghe,
        Hoadon,
        Hoadonphat,
        Lichchieu,
        Mathang,
        Phim,
        Phongchieu,
        Rapchieu,
        Ve,
        Khachhang,
        Nhanvien,
        Nguoidung,
        Nguoi,
        Ghedat
      ],
      host: process.env.DATABASE_HOST || '',
      port: parseInt(process.env.DATABASE_PORT, 10) || 5432,
      username: process.env.DATABASE_USERNAME || '',
      password: process.env.DATABASE_PASSWORD || '',
      database: process.env.DATABASE_NAME || '',
      synchronize: true,
    }),
    TypeOrmModule.forFeature([
      Quanly,
      Dichvusudung,
      Ghe,
      Hoadon,
      Hoadonphat,
      Lichchieu,
      Mathang,
      Phim,
      Phongchieu,
      Rapchieu,
      Ve,
      Khachhang,
      Nhanvien,
      Nguoidung,
      Nguoi,
      Ghedat
    ]),
  ],
  controllers: [AppController],
  providers: [
    AppService,
    NhanvienService,
    LichchieuService,
    PhimService,
    PhongchieuService,
    GheService,
    GhedatService,
    VeService,
    HoadonService,
    KhachhangService,
    HoadonphatService,
    Repository,
  ],
})
export class AppModule {
}
