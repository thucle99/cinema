import { Entity, Column } from 'typeorm';
import { Exclude } from 'class-transformer';
import { Nguoi } from './Nguoi';

@Entity({ name: 'tblNguoidung' })
export class Nguoidung extends Nguoi {
  @Column({default:null})
  taikhoan: string;

  @Exclude() 
  @Column()   
  matkhau: string;

  @Column({default:null})
  chinhanh: string;

  @Column({default:null})
  vitri: string;
}
