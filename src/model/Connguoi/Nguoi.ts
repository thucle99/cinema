import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity({ name: 'tblNguoi' })
export class Nguoi {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  hoten: string;

  @Column({default:null})
  ngaysinh: Date;

  @Column({default:null})
  diachi: string;

  @Column()
  email: string;

  @Column()
  sodienthoai: string;

  @Column({default:null})
  soCMT: string;
}
