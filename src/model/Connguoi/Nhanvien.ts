import { Entity, OneToMany } from 'typeorm';
import { Hoadon } from '../RapPhim/Hoadon';
import { Hoadonphat } from '../RapPhim/Hoadonphat';
import { Nguoidung } from './Nguoidung';

@Entity({ name: 'tblNhanvien' })
export class Nhanvien extends Nguoidung {
    @OneToMany(() => Hoadon, (hoadon) => hoadon.nhanvien)
    listHoadon: Hoadon[];

    @OneToMany(() => Hoadonphat, (hoadon) => hoadon.nhanvien)
    listHoadonphat: Hoadonphat[];
}
