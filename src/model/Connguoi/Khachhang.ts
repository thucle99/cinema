import { Column, Entity, OneToMany } from 'typeorm';
import { Hoadon } from '../RapPhim/Hoadon';
import { Nguoi } from './Nguoi';

@Entity({ name: 'tblKhachhang' })
export class Khachhang extends Nguoi {
  @Column()
  diemtichluy: number;

  @OneToMany(() => Hoadon, (hoadon) => hoadon.khachhang)
  listHoadon: Hoadon[];
}
