import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import { Dichvusudung } from './Dichvusudung';

@Entity({ name: 'tblMathang' })
export class Mathang {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  ten: string;

  @Column()
  loai: string;

  @Column()
  size: string;

  @Column()
  gia: number;

  @OneToMany(() => Dichvusudung, (dv) => dv.mathang)
  listDichvu: Dichvusudung[];
}
