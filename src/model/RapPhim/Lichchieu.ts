import { Column, Entity, JoinColumn, ManyToOne, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import { Ghedat } from './Ghedat';
import { Phim } from './Phim';
import { Phongchieu } from './Phongchieu';
import { Ve } from './Ve';

@Entity({ name: 'tblLichchieu' })
export class Lichchieu {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  ngaygiochieu: Date;

  @Column()
  loaisuatchieu: string;

  @ManyToOne(() => Phim, (phim) => phim.listLichchieu)
  phim: Phim;

  @ManyToOne(() => Phongchieu, (phongchieu) => phongchieu.listLichchieu)
  phongchieu: Phongchieu;

  @OneToMany(() => Ghedat, (datghe) => datghe.ghe)
  listGhedat: Ghedat[];
}
