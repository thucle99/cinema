import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import { Lichchieu } from './Lichchieu';

@Entity({ name: 'tblPhim' })
export class Phim {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  tenphim: string;

  @Column()
  loaiphim: string;

  @Column()
  ngaysanxuat: Date;

  @Column()
  mota: string;

  @OneToMany(() => Lichchieu, (lichChieu) => lichChieu.phim)
  listLichchieu: Lichchieu[];
}
