import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import { Nhanvien } from '../Connguoi/Nhanvien';
import { Hoadon } from './Hoadon';

@Entity({ name: 'tblHoadonphat' })
export class Hoadonphat{
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  soluongvetra: number;

  @Column()
  tienphat: number;

  @Column()
  tienhoanlai: number;

  @Column()
  thoigianhoanve: Date;

  @ManyToOne(() => Hoadon, (hoadon) => hoadon.listHoadonphat)
  hoadon: Hoadon;

  @ManyToOne(() => Nhanvien, (nhanvien) => nhanvien.listHoadonphat)
  nhanvien: Nhanvien;
}
