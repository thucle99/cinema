import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import { Ghe } from './Ghe';
import { Lichchieu } from './Lichchieu';

@Entity({ name: 'tblGhedat' })
export class Ghedat {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  trangthai: string;

  @ManyToOne(() => Ghe, (ghe) => ghe.listGhedat)  // phong chieu co nhieu ghe
  ghe: Ghe;

  @ManyToOne(() => Lichchieu, (lichchieu) => lichchieu.listGhedat)  // phong chieu co nhieu ghe
  lichchieu: Lichchieu;
  
}
