import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import { Hoadon } from './Hoadon';
import { Mathang } from './Mathang';

@Entity({ name: 'tblDichvuanuong' })
export class Dichvusudung {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  uudai: number;

  @ManyToOne(() => Mathang, (mathang) => mathang.listDichvu)
  mathang: Mathang;

  @ManyToOne(() => Hoadon, (hoadon) => hoadon.listDichvu)
  hoadon: Hoadon;
}
