import {
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  OneToMany,
  OneToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { Ghe } from './Ghe';
import { Ghedat } from './Ghedat';
import { Hoadon } from './Hoadon';
import { Lichchieu } from './Lichchieu';

@Entity({ name: 'tblVe' })
export class Ve {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  giave: number;

  @Column()
  loaive: string;

  @Column({default:"trống"})
  uudai: string;

  @ManyToOne(() => Hoadon, (hoadon) => hoadon.listVe)
  hoadon: Hoadon;

  @JoinColumn()
  @OneToOne(() => Ghedat)
  ghedat: Ghedat;
}
