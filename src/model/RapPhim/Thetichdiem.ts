import {
  Column,
  Entity,
  JoinColumn,
  OneToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { Khachhang } from '../Connguoi/Khachhang';

@Entity({ name: 'tblThetichdiem' })
export class Thetichdiem {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  diem: number;

  @OneToOne(() => Khachhang)
  @JoinColumn()
  khachhang: Khachhang;
}
