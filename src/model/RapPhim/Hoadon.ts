import { Column, Entity, ManyToOne, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import { Khachhang } from '../Connguoi/Khachhang';
import { Nhanvien } from '../Connguoi/Nhanvien';
import { Dichvusudung } from './Dichvusudung';
import { Hoadonphat } from './Hoadonphat';
import { Ve } from './Ve';

@Entity({ name: 'tblHoadon' })
export class Hoadon {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  soluong: number;

  @Column()
  tienmat: number;
  
  @Column()
  diemtichluy: number;

  @Column()
  tongtien: number;

  @Column()
  thoigianban: Date;

  @OneToMany(() => Ve, (ve) => ve.hoadon)
  listVe: Ve[];

  @OneToMany(() => Dichvusudung, (dichvu) => dichvu.hoadon)
  listDichvu: Dichvusudung[];

  @OneToMany(() => Hoadonphat, (hoadonphat) => hoadonphat.hoadon)
  listHoadonphat: Hoadonphat[];

  @ManyToOne(() => Nhanvien, (nhanvien) => nhanvien.listHoadon)
  nhanvien: Nhanvien;

  @ManyToOne(() => Khachhang, (khachhang) => khachhang.listHoadon)
  khachhang: Khachhang;
}
