import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import { Phongchieu } from './Phongchieu';

@Entity({ name: 'tblRapchieu' })
export class Rapchieu {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  ten: string;

  @Column()
  diachi: string;

  @Column()
  gioithieu: string;

  @OneToMany(() => Phongchieu, (phongchieu) => phongchieu.rapchieu)
  listPhongchieu: Phongchieu[];
  // Rap chieu chua thong tin cua phong chieu
}
