import {
  Column,
  Entity,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { Ghe } from './Ghe';
import { Lichchieu } from './Lichchieu';
import { Rapchieu } from './Rapchieu';

@Entity({ name: 'tblPhongchieu' })
export class Phongchieu {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  soluongghe: number;

  @Column()
  dacdiemphong: string;

  @Column()
  tenphong: string;

  @OneToMany(() => Ghe, (ghe) => ghe.phongchieu)
  listGhe: Ghe[];

  @ManyToOne(() => Rapchieu, (rapchieu) => rapchieu.listPhongchieu)
  rapchieu: Rapchieu;

  // Phong chieu thuoc Rap chieu

  @OneToMany(() => Lichchieu, (lichChieu) => lichChieu.phongchieu)
  listLichchieu: Lichchieu[];
}
