import { Column, Entity, ManyToOne, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import { Ghedat } from './Ghedat';
import { Phongchieu } from './Phongchieu';

@Entity({ name: 'tblGhe' })
export class Ghe {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  loaighe: string;

  @Column()
  hang: string;

  @Column()
  cot: number;

  @ManyToOne(() => Phongchieu, (phongchieu) => phongchieu.listGhe)  // phong chieu co nhieu ghe
  phongchieu: Phongchieu;

  @OneToMany(() => Ghedat, (ghedat) => ghedat.ghe)
  listGhedat: Ghedat[];
}
