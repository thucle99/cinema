import { BadRequestException } from '@nestjs/common';
import { Test, TestingModule } from '@nestjs/testing';
import { AppController } from '../app.controller';
import { GheService } from '../service/Ghe.service';

describe('AppController', () => {
  let gheService: GheService;
  const mockGheService = {}

  beforeEach(async () => {
    const app: TestingModule = await Test.createTestingModule({
      controllers: [],
      providers: [GheService],
    }).overrideProvider(GheService).useValue(mockGheService).compile();

    gheService = app.get<GheService>(GheService);
  });

  describe('kiemtrandangnhap', () => {
    it('should return true if taikhoan, matkhau is match', async () => {
      // expect(nhanvienService.kiemtraDangnhap({taikhoan:"thanle93",matkhau:"123456"})).toBe('chao');

      try {
        // await nhanvienService.kiemtraDangnhap({ taikhoan: "thanle93", matkhau: "123456" });
        // await gheService;
      } catch (err) {
        expect(err).toBeInstanceOf(BadRequestException);
        expect(err.message).toEqual('Tài khoản hoặc mật khẩu không chính xác.');
      }

    });
  });
});