import { BadRequestException } from '@nestjs/common';
import { Test, TestingModule } from '@nestjs/testing';
import { getRepositoryToken } from '@nestjs/typeorm';
import { Phim } from '../model/RapPhim/Phim';
import { Repository } from 'typeorm';
import { PhimService } from '../service/Phim.service';

type MockType<T> = {
  [P in keyof T]?: jest.Mock<{}>;
};
const fakePhim = {
  id:"1",
};


const repositoryMockFactory: () => MockType<Repository<any>> = jest.fn(() => ({
  find: jest.fn((entity) => [fakePhim, fakePhim]),
  // taọ ra function , làm giả hàm findOne bên Service
}));

describe('AppController', () => {
  let phimService: PhimService;
  const mockPhimService = {}

  beforeEach(async () => {
    const app: TestingModule = await Test.createTestingModule({
      controllers: [],
      providers: [
        PhimService,
        {
          provide: getRepositoryToken(Phim),
          useFactory: repositoryMockFactory,
        },
      ],
    }).overrideProvider(PhimService).useValue(mockPhimService).compile();
    phimService = app.get<PhimService>(PhimService);
  });

  describe('getPhim', () => {
    it('return list phim when Staff is login', async () => {
      try {
        await phimService.getListPhim();
      } catch (err) {
        expect(err).toBeInstanceOf(BadRequestException);
        expect(err.message).toEqual('Bạn cần đăng nhập vào hệ thống để xem danh sách phim.');
      }

    });
  });
});