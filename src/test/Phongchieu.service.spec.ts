import { BadRequestException } from '@nestjs/common';
import { Test, TestingModule } from '@nestjs/testing';
import { PhongchieuService } from '../service/Phongchieu.service'

describe('AppController', () => {
  let phongchieuService: PhongchieuService;
  const mockPhongchieuService = {}

  beforeEach(async () => {
    const app: TestingModule = await Test.createTestingModule({
      controllers: [],
      providers: [PhongchieuService],
    }).overrideProvider(PhongchieuService).useValue(mockPhongchieuService).compile();
    phongchieuService = app.get<PhongchieuService>(PhongchieuService);
  });

  describe('kiemtrandangnhap', () => {
    it('should return true if taikhoan, matkhau is match', async () => {
      try {
        // await phimService.kiemtraDangnhap({ taikhoan: "thanle93", matkhau: "123456" });
      } catch (err) {
        expect(err).toBeInstanceOf(BadRequestException);
        expect(err.message).toEqual('Tài khoản hoặc mật khẩu không chính xác.');
      }

    });
  });
});