import { BadRequestException } from '@nestjs/common';
import { Test, TestingModule } from '@nestjs/testing';
import { LichchieuService } from '../service/Lichchieu.service'

describe('AppController', () => {
  let lichchieuService: LichchieuService;
  const mockLichchieuService = {}

  beforeEach(async () => {
    const app: TestingModule = await Test.createTestingModule({
      controllers: [],
      providers: [LichchieuService],
    }).overrideProvider(LichchieuService).useValue(mockLichchieuService).compile();
    lichchieuService = app.get<LichchieuService>(LichchieuService);
  });

  describe('kiemtrandangnhap', () => {
    it('should return true if taikhoan, matkhau is match', async () => {
      try {
        // await phimService.kiemtraDangnhap({ taikhoan: "thanle93", matkhau: "123456" });
      } catch (err) {
        expect(err).toBeInstanceOf(BadRequestException);
        expect(err.message).toEqual('Tài khoản hoặc mật khẩu không chính xác.');
      }

    });
  });
});