import { BadRequestException } from '@nestjs/common';
import { Test, TestingModule } from '@nestjs/testing';
import { Nhanvien } from '../model/Connguoi/Nhanvien';
import { getRepositoryToken } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { NhanvienService } from '../service/Nhanvien.service';

type MockType<T> = {
  [P in keyof T]?: jest.Mock<{}>;
};

const repositoryMockFactory: () => MockType<Repository<any>> = jest.fn(() => ({
  findOne: jest.fn((entity) => entity),
  // taọ ra function , làm giả hàm findOne bên Service
}));

describe('AppController', () => {
  let nhanvienService: NhanvienService;

  beforeEach(async () => {
    const app: TestingModule = await Test.createTestingModule({
      controllers: [],
      providers: [
        NhanvienService,
        {
          provide: getRepositoryToken(Nhanvien),
          useFactory: repositoryMockFactory,
        },
      ],
    }).compile();
    nhanvienService = app.get<NhanvienService>(NhanvienService);
  });

  describe('kiemtrandangnhap', () => {
    it('taikhoan not match, matkhau is match', async () => {
      try {
        await nhanvienService.kiemtraDangnhap({ taikhoan: "thanle99", matkhau: "123456" });
      } catch (err) {
        expect(err).toBeInstanceOf(BadRequestException);
        expect(err.message).toEqual('Tài khoản hoặc mật khẩu không chính xác.');
      }
    });

    it('taikhoan is match, matkhau not match', async () => {
      try {
        await nhanvienService.kiemtraDangnhap({ taikhoan: "thanle93", matkhau: "123457" });
      } catch (err) {
        expect(err).toBeInstanceOf(BadRequestException);
        expect(err.message).toEqual('Tài khoản hoặc mật khẩu không chính xác.');
      }
    });

    it('taikhoan is null, matkhau is match', async () => {
      try {
        await nhanvienService.kiemtraDangnhap({ taikhoan: "", matkhau: "123456" });
      } catch (err) {
        expect(err).toBeInstanceOf(BadRequestException);
        expect(err.message).toEqual('Tài khoản hoặc mật khẩu không chính xác.');
      }
    });

    it('taikhoan is match, matkhau is null', async () => {
      try {
        await nhanvienService.kiemtraDangnhap({ taikhoan: "thanle93", matkhau: "" });
      } catch (err) {
        expect(err).toBeInstanceOf(BadRequestException);
        expect(err.message).toEqual('Tài khoản hoặc mật khẩu không chính xác.');
      }
    });

    it('taikhoan is null, matkhau is null', async () => {
      try {
        await nhanvienService.kiemtraDangnhap({ taikhoan: "", matkhau: "" });
      } catch (err) {
        expect(err).toBeInstanceOf(BadRequestException);
        expect(err.message).toEqual('Tài khoản hoặc mật khẩu không chính xác.');
      }
    });

  });

});