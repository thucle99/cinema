let thoigianTrave = document.getElementsByClassName('thoigian-trave')[0];
let tienphat = document.getElementsByClassName('tienphat')[0];
let tienhoadon = document.getElementsByClassName('tienhoadon')[0];

let tienhoantra = document.getElementsByClassName('tienhoantra')[0];
let inhoadon = document.getElementsByClassName('inhoadon')[0];
let xacnhanInhoadon = document.getElementsByClassName('xacnhanInhoadon')[0];
let success = document.getElementsByClassName('success')[0];
let taikhoan = document.getElementsByClassName('taikhoan')[0];
let dangxuat = document.getElementsByClassName('dangxuat')[0];

success.style.display = "none"
thoigianTrave.textContent = formatDate(new Date);

function formatDate(date) {
    var d = new Date(date)
    return formatNumber0(d.getHours()) + ":" + formatNumber0(d.getMinutes()) + " Ngày " +
        formatNumber0(d.getDate()) + "/" + formatNumber0(d.getMonth()) + "/" + d.getFullYear();
}

function formatNumber0(data) {
    if (data < 10) {
        return "0" + data;
    }
    else return data
}

function handleCash() {
    tienhoantra.textContent = tienhoadon.textContent - tienphat.textContent;
    tienhoadon.textContent = formatCash(tienhoadon.textContent.trim())
    tienphat.textContent = formatCash(tienphat.textContent.trim());
    tienhoantra.textContent = formatCash(tienhoantra.textContent.trim())
}

function formatCash(str) {
    return str.split('').reverse().reduce((prev, next, index) => {
        return ((index % 3) ? next : (next + ',')) + prev
    })
}
handleCash();

if (localStorage.getItem('taikhoan')) taikhoan.textContent = localStorage.getItem('taikhoan')
else window.location = `http://localhost:3000/dang-nhap`;

dangxuat.addEventListener("click", function (e) {
    localStorage.removeItem("taikhoan")
})

inhoadon.addEventListener("click", function (e) {
    setTimeout(() => {
        success.style.display = "block"
    }, 300)

    setTimeout(() => {
        success.style.display = "none";
    }, 1000)
})
