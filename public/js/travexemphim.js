var selectPhim = document.getElementsByClassName('select-phim')[0];
var selectPhongChieu = document.getElementsByClassName('select-phong-chieu')[0];
var ngaygiochieu = document.getElementsByClassName('ngaygiochieu');
let taikhoan = document.getElementsByClassName('taikhoan')[0];
let dangxuat = document.getElementsByClassName('dangxuat')[0];

for (let i = 0; i < ngaygiochieu.length; i++) {
   ngaygiochieu[i].textContent = formatDate(ngaygiochieu[i].textContent)
}

selectPhim.addEventListener("click", function (e) {
   window.location = `http://localhost:3000/tra-ve-xem-phim?phimId=${e.target.value}`;
})

selectPhongChieu.addEventListener("click", function (e) {
   window.location = `http://localhost:3000/tra-ve-xem-phim?phongchieuId=${e.target.value}`;
})

function formatDate(date) {
   var d = new Date(date)
   return formatNumber0(d.getHours()) + ":" + formatNumber0(d.getMinutes()) + " Ngày " +
      formatNumber0(d.getDate()) + "/" + formatNumber0(d.getMonth()) + "/" + d.getFullYear();
}

function formatNumber0(data) {
   if (data < 10) {
      return "0" + data;
   }
   else return data
}

if (localStorage.getItem('taikhoan')) taikhoan.textContent = localStorage.getItem('taikhoan')
else window.location = `http://localhost:3000/dang-nhap`;

dangxuat.addEventListener("click", function (e) {
   localStorage.removeItem("taikhoan")
})
