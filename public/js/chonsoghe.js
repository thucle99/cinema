let listGhe = document.getElementsByClassName('table-body')[0];
let btnTrave = document.getElementsByClassName('btn-trave')[0];
let taikhoan = document.getElementsByClassName('taikhoan')[0];
let dangxuat = document.getElementsByClassName('dangxuat')[0];

listGhe.addEventListener("click", function (e) {
    let ghedatId = e.target.childNodes[1].className
    window.location = `${window.location.href.slice(0, 78)}&hang=${e.target.className[0]}&cot=${e.target.className[1]}&ghedatId=${ghedatId}`
}, true)

btnTrave.addEventListener("click", function (e) {
    let ghedatId = window.location.href.slice(101)
    window.location = `http://localhost:3000/tra-ve-xem-phim/hoa-don-phat?ghedatId=${ghedatId}&taikhoan=${localStorage.getItem('taikhoan')}`
})

if (localStorage.getItem('taikhoan')) taikhoan.textContent = localStorage.getItem('taikhoan')
else window.location = `http://localhost:3000/dang-nhap`;

dangxuat.addEventListener("click", function (e) {
    localStorage.removeItem("taikhoan")
})
